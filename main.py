from datetime import timedelta

from helper import *

path = "input_data/data_sample.csv"

data = get_df_from_csv(path)

# Define the start date and end date
start_date = datetime(2022, 7, 16) - timedelta(days=100)
end_date = datetime(2022, 7, 16)

# Calculate the same dates from last year
start_date_last_year = start_date - timedelta(days=365)
end_date_last_year = end_date - timedelta(days=365)

df_current_year = get_date_interval(data, start_date, end_date)
df_last_year = get_date_interval(data, start_date_last_year, end_date_last_year)

df_unique_count = get_count_of_unique_values(df_current_year)
df_unique_count_last_year = get_count_of_unique_values(df_last_year)

# save_df_to_csv(df_unique_count, "results/res_1.csv")
# save_df_to_csv(df_unique_count_last_year, "results/res_2.csv")

df_unique_count_last_year = change_year_in_df(df_unique_count_last_year, year=2022)
merged_data = merge_2_dfs(df_unique_count, df_unique_count_last_year, start_date=start_date, end_date=end_date)

# save_df_to_csv(merged_data, "results/merged_data.csv")

plot_booking_curve(merged_data, start_date=start_date, end_date=end_date)
