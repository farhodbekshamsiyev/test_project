from datetime import datetime

import pandas as pd
from matplotlib import pyplot as plt
from pandas import DataFrame


def get_df_from_csv(filename: str = "input_data/data_sample.csv") -> DataFrame:
    # read csv file from path and return as dataframe
    return pd.read_csv(filename, header=0)


def get_date_interval(df: DataFrame, start_date: datetime, end_date: datetime,
                      col_name: str = 'DATE_OF_RESERVATION') -> DataFrame:
    # extract dataframe by given date interval
    df[col_name] = pd.to_datetime(df[col_name])
    return df[(start_date <= df[col_name]) & (df[col_name] <= end_date)]


def get_count_of_unique_values(df: DataFrame, **kwargs) -> DataFrame:
    # get count of unique reserved room id for each day
    reserved_date = kwargs.get('reserved_date', 'DATE_OF_RESERVATION')
    reserved_room_id = kwargs.get('reserved_room_id', 'ROOM_RESERVATION_ID')
    return df.groupby(reserved_date)[reserved_room_id].nunique().reset_index()


def save_df_to_csv(df: DataFrame, path: str = "results/results.csv") -> None:
    # Saving dataframe
    df.to_csv(path, index=False)


def change_year_in_df(df: DataFrame, **kwargs) -> DataFrame:
    # in order to plot two years data I need to combine dfs and need to change df's year
    reserved_date = kwargs.get('reserved_date', 'DATE_OF_RESERVATION')
    year = kwargs.get('year', 2022)
    df[reserved_date] = df[reserved_date].apply(
        lambda x: x.replace(year=year))
    return df


def merge_2_dfs(df1: DataFrame, df2: DataFrame, **kwargs) -> DataFrame:
    # combining two dfs into one and assigning new columns names
    start_date = kwargs.get('start_date', '2022-07-16')
    end_date = kwargs.get('end_date', '2022-04-07')
    date_range = pd.date_range(start=start_date, end=end_date)

    df = date_range.to_frame(name='DATE_OF_RESERVATION').merge(
        df1, on='DATE_OF_RESERVATION', how='left'
    ).merge(
        df2, on='DATE_OF_RESERVATION', how='left'
    )
    df.columns = ['Date', 'Current Year', 'Last Year']
    df['Current Year'] = df['Current Year'].fillna(0)
    df['Last Year'] = df['Last Year'].fillna(0)
    return df


def plot_booking_curve(df: DataFrame, **kwargs) -> None:
    date = kwargs.get('date', 'Date')
    current_year = kwargs.get('current_year', 'Current Year')
    last_year = kwargs.get('last_year', 'Last Year')
    start_date = kwargs.get('start_date', '2022-07-16')
    end_date = kwargs.get('end_date', '2022-04-07')

    plt.figure(figsize=(10, 6))  # Adjust the figure size as needed
    plt.plot(df[date], df[current_year], linestyle='-',
             color='b')
    plt.plot(df[date], df[last_year], linestyle='-',
             color='r')

    plt.title('Booking Curve')
    plt.legend(['Current Year', 'Last Year'])

    plt.xlabel('Date')
    plt.ylabel('Reserved Rooms in %')

    plt.xticks([start_date, end_date])
    plt.yticks([i for i in range(0, 101, 10)], labels=[f"{i}%" for i in range(0, 101, 10)])

    plt.savefig('results/booking_curve.png')
    plt.show()
